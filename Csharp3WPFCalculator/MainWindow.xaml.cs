﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Csharp3WPFCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private long number1 = 0;
        private long number2 = 0;
        private string operation = "";

        public MainWindow()
        {
            InitializeComponent();
            this.KeyDown += new KeyEventHandler(Window_KeyDown);
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            NumberKey1();
            btnEq.Focus();
        }

        private void NumberKey1()
        {
            number1 = (number1 * 10) + 1;
            txtDisplay.Text = number1.ToString();
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            NumberKey3();
            btnEq.Focus();
        }

        private void NumberKey3()
        {
            number1 = (number1 * 10) + 3;
            txtDisplay.Text = number1.ToString();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            NumberKey2();
            btnEq.Focus();
        }

        private void NumberKey2()
        {
            number1 = (number1 * 10) + 2;
            txtDisplay.Text = number1.ToString();
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            NumberKey4();
            btnEq.Focus();
        }

        private void NumberKey4()
        {
            number1 = (number1 * 10) + 4;
            txtDisplay.Text = number1.ToString();
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            NumberKey5();
            btnEq.Focus();
        }

        private void NumberKey5()
        {
            number1 = (number1 * 10) + 5;
            txtDisplay.Text = number1.ToString();
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            NumberKey6();
            btnEq.Focus();
        }

        private void NumberKey6()
        {
            number1 = (number1 * 10) + 6;
            txtDisplay.Text = number1.ToString();
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            NumberKey7();
            btnEq.Focus();
        }

        private void NumberKey7()
        {
            number1 = (number1 * 10) + 7;
            txtDisplay.Text = number1.ToString();
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            NumberKey8();
            btnEq.Focus();
        }

        private void NumberKey8()
        {
            number1 = (number1 * 10) + 8;
            txtDisplay.Text = number1.ToString();
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            NumberKey9();
            btnEq.Focus();
        }

        private void NumberKey9()
        {
            number1 = (number1 * 10) + 9;
            txtDisplay.Text = number1.ToString();
        }

        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            NumberKey0();
            btnEq.Focus();
        }

        private void NumberKey0()
        {
            number1 = (number1 * 10);
            txtDisplay.Text = number1.ToString();
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            Addition();
            btnEq.Focus();
        }

        private void Addition()
        {
            operation = "+";
            txtDisplay.Text = "0";
            number2 = number1;
            number1 = 0;
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            Substract();
            btnEq.Focus();
        }

        private void Substract()
        {
            operation = "-";
            txtDisplay.Text = "0";
            number2 = number1;
            number1 = 0;
        }

        private void btnMult_Click(object sender, RoutedEventArgs e)
        {
            Multiply();
            btnEq.Focus();
        }

        private void Multiply()
        {
            operation = "*";
            txtDisplay.Text = "0";
            number2 = number1;
            number1 = 0;
        }

        private void btnDiv_Click(object sender, RoutedEventArgs e)
        {
            Divide();
            btnEq.Focus();
        }

        private void Divide()
        {
            operation = "/";
            txtDisplay.Text = "0";
            number2 = number1;
            number1 = 0;
        }

        private void btnEq_Click(object sender, RoutedEventArgs e)
        {
            Equal();
            btnEq.Focus();
        }

        private void Equal()
        {
            switch (operation)
            {
                case "+":
                    txtDisplay.Text = (number2 + number1).ToString();
                    break;

                case "-":
                    txtDisplay.Text = (number2 - number1).ToString();
                    break;

                case "*":
                    txtDisplay.Text = (number2 * number1).ToString();
                    break;

                case "/":
                    txtDisplay.Text = (number2 / number1).ToString();
                    break;
            }
            operation = "";
            number1 = Convert.ToInt64(txtDisplay.Text);
        }

        private void btnCentry_Click(object sender, RoutedEventArgs e)
        {
            ClearEntry();
            btnEq.Focus();
        }

        private void ClearEntry()
        {
            if (operation == "")
            {
                number1 = 0;
            }
            else
            {
                number2 = 0;//Fix this
            }
            txtDisplay.Text = "0";
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
            btnEq.Focus();
        }

        private void Clear()
        {
            number1 = 0;
            number2 = 0;
            operation = "";
            txtDisplay.Text = "0";
        }

        private void btnBackspace_Click(object sender, RoutedEventArgs e)
        {
            Backspace();
            btnEq.Focus();
        }

        private void Backspace()
        {
            if (operation == "")
            {
                number1 = (number1 / 10);
                txtDisplay.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 / 10);
                txtDisplay.Text = number2.ToString();
            }
        }

        private void btnPositiveNegative_Click(object sender, RoutedEventArgs e)
        {
            ChangeSign();
            btnEq.Focus();
        }

        private void ChangeSign()
        {
            if (operation == "")
            {
                number1 *= -1;
                txtDisplay.Text = number1.ToString();
            }
            else
            {
                number2 *= -1;
                txtDisplay.Text = number2.ToString();
            }
        }

        //private void Window_Loaded(object sender, RoutedEventArgs e) => this.KeyDown += new KeyEventHandler(Window_KeyDown);

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((e.Key == Key.Subtract))
            {
                Substract();
            }
            if ((e.Key == Key.Add))
            {
                Addition();
            }
            if ((e.Key == Key.Multiply))
            {
                Multiply();
            }
            if ((e.Key == Key.Divide))
            {
                Divide();
            }
            if ((e.Key == Key.NumPad0))
            {
                NumberKey0();
            }
            if ((e.Key == Key.NumPad1))
            {
                NumberKey1();
            }
            if ((e.Key == Key.NumPad2))
            {
                NumberKey2();
            }
            if ((e.Key == Key.NumPad3))
            {
                NumberKey3();
            }
            if ((e.Key == Key.NumPad4))
            {
                NumberKey4();
            }
            if ((e.Key == Key.NumPad5))
            {
                NumberKey5();
            }
            if ((e.Key == Key.NumPad6))
            {
                NumberKey6();
            }
            if ((e.Key == Key.NumPad7))
            {
                NumberKey7();
            }
            if ((e.Key == Key.NumPad8))
            {
                NumberKey8();
            }
            if ((e.Key == Key.NumPad9))
            {
                NumberKey9();
            }
            if ((e.Key == Key.Back))
            {
                Backspace();
            }
            if ((e.Key == Key.Enter))
            {
                Equal();
            }

            e.Handled = true;
        }
    }
}